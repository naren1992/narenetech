<?php
if ( ! function_exists( 'narenetech_setup' ) ) :

function narenetech_setup() {
 

    load_theme_textdomain( 'narenetech', get_template_directory() . '/languages' );

    add_theme_support( 'automatic-feed-links' );
 

    add_theme_support( 'post-thumbnails' );
    
    add_theme_support( 'custom-logo' );
 

    register_nav_menus( array(
        'primary'   => __( 'Primary Menu', 'narenetech' ),
        'secondary' => __('Secondary Menu', 'narenetech' )
    ) );
 
    add_theme_support( 'post-formats', array ( 'aside', 'gallery', 'quote', 'image', 'video' ) );
}
endif; // narenetech_setup
add_action( 'after_setup_theme', 'narenetech_setup' );

require get_template_directory().'/inc/enqueue.php';