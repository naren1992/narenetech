<?php


// Enqueue scripts and styles
function narenetech_style_scripts() {

	// Bootstrap Style
	wp_enqueue_style('bootstrap-style',get_template_directory_uri().'/assets/css/bootstrap.min.css');

	// all_plugins Style
	wp_enqueue_style('all-plugins-style',get_template_directory_uri().'/assets/css/all_plugin.css');

	// Project Custom Style
	wp_enqueue_style('project-custom-style',get_template_directory_uri().'/assets/css/project_custom.css');

	// Stylesheet
 	wp_enqueue_style( 'style-name', get_stylesheet_uri() );


 	// Scripts

 	// jquery js
 	wp_enqueue_script( 'jquery-js','http://code.jquery.com/jquery-1.11.3.js', array(), '1.0.0', true );

 	// bootstrap js
 	wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array(), '1.0.0', true );

 	// mmenu js
 	wp_enqueue_script( 'mmenu-js', get_template_directory_uri() . '/assets/js/jquery.mmenu.min.all.js', array(), '1.0.0', true );
 	// project custom js
 	wp_enqueue_script( 'project-custom-js', get_template_directory_uri() . '/assets/js/project_custom.js', array(), '1.0.0', true );
 
}
add_action('wp_enqueue_scripts', 'narenetech_style_scripts');