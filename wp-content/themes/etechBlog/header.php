<!DOCTYPE html>
<html>
<head>
	<title>
		<?php 
			if(is_front_page() || is_home()){
		        echo get_bloginfo('name');
		    } else{
		        wp_title(''); echo ' | ';  bloginfo( 'name' );
		    } 
    	?>
    </title>

	<?php wp_head(); ?>
</head>
<body>
<div class='main_page'>
	<header class='main_header abc'>
		<div class="nav_bar">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<div class="top_logo">
							<?php the_custom_logo(); ?>
						</div><!-- End .top_logo -->
					</div><!-- End .col-md-3 -->
					<div class="col-md-9">
						<div class='top_nav'>
							<?php
								wp_nav_menu(
									array(
										'menu' => 'Top Menu'
									)
								);
							?>
						</div><!-- End .top_nav -->
					</div><!-- End .col-md-9 -->
				</div><!-- End .row -->
			</div><!-- End .container -->
		</div><!-- End .nav_bar -->
	</header><!-- Header -->	
	<!-- <div class="mo_header abc">
        <ul class="mo_ul">
            <li>
            	<a href="#" class="mo_logo">
                <?php //the_custom_logo(); ?>            
            </li>
            <li><a href="#mobile_menu" class="mo_menu">Menu</a></li>
        </ul>
    </div> --> <!-- end mo_header -->
    <!-- <nav id="mobile_menu"> -->
	       
	<?php
		// wp_nav_menu(
		// 	array(
		// 		'menu' => 'Top Menu'
		// 	)
		// );
	?>		

    <!--</nav> end mobile_menu -->


