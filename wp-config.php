<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'narenetech' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'mJHtb||VpBZ6Hiz?#nU`$gQBN*bIDy_d3m(qlG@oI)tfSSB;]6`JYqS9ODcE5eg]' );
define( 'SECURE_AUTH_KEY',  '|yK45@<B%8r)NRh((KYjOxr[r.Py[w{>Lyv(oY&cgdK7Gr]8574-=z>yGx>V`@M}' );
define( 'LOGGED_IN_KEY',    'fmWE*}2exj=+x<AO}}/:P}N!iy^PeniAo?CiCbr.G}wv?Ir,~p/P0%q1`+9Jqeh7' );
define( 'NONCE_KEY',        'VOPxcF,$`bgYuF2.jp><+D9%a6Lum5zVs(idCttIx(%$D_fu;nq[^7du}h=oL,F+' );
define( 'AUTH_SALT',        '=BzX?^gaj.Xd%#Sv7~V&+yR4pD[nRY%>fvmC=2LMf$f2xcV3tY[;0TF!tc{i6cD1' );
define( 'SECURE_AUTH_SALT', '&7]oIsV$]6YCC;31l!?5j0/43cUEkW:2C_}w,5C[@-!dC|Pyk>9bx|.Qoh/x>HrY' );
define( 'LOGGED_IN_SALT',   'I~a; <V@tXG8ETZ^d}X]eq<cUZ*Ak]*j<^oid&2C-.fK8lZ.Y40XknrD,oM2j{ A' );
define( 'NONCE_SALT',       '5 1PNVfMLm(aip0AExZjM3=3nhh[ `Sy1(tm-97sE1;uWm;Hu;,Zl+/pDG*h:-=8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

